# STATIC WEBSITE MODULE

## What is this?
A terraform module which create static website with S3 and Cloudfront.

This module will use S3 website endpoint as origin in Cloudfront distribution instead of S3 Rest API endpoint.
Access to S3 will be restricted by a Referer header.  
## Usage

```hcl
module "static_website" {
  source  = "gitlab.com/terraform-modules21/static-website/aws"
  version = "1.0.0"

  s3_bucket_name = "some-name"
}
```

## Custom domain
You will either need a certificate for your custom domain issued by ACM or imported to ACM in us-east-1 region.

```hcl
  domain_name                 = "my.domain.com"
  acm_certificate_domain_name = "*.domain.com"
```

If you also use Route53

```hcl
  route53_hosted_zone_name = "domain.com"
```

## Custom cache policy

You will need to create Cloudfront cache policy outside this module.
```hcl
  cloudfront_cache_policy_id = "your_cache_policy_id"
```

## Input Variables
- `s3_bucket_name` - (Required) Name for S3 bucket.
- `price_class` - Cloudfront distribution price class. Either PriceClass_All, PriceClass_200 or PriceClass_100.
- `domain_name` - Custom domain name for website.  Will require acm_certificate_arn.
- `route53_hosted_zone_name` - Name of Route53 hosted zone. Need it to find hosted zone id.
- `acm_certificate_domain_name` - Domain name that is issued with ACM. It's required if domain_name is specified. Note: Cloudfront required the ACM resource to be in us-east-1 region.
- `cloudfront_cache_policy_id` - Cloudfront cache policy id. Default is the Managed-CachingOptimized policy managed by AWS.
- `http_version` - The maximum HTTP version to support on the distribution. Allowed values are http1.1, http2, http2and3 and http3

## Outputs

- `s3_website_endpoint` - S3 bucket website endpoint.
- `cloudfront_dns` - Cloudfront domain name.
- `cloudfront_id` - Id of Cloudfront distribution.
- `s3_bucket_name` - S3 bucket name.


Authors
=======

phunguyen9297@gmail.com