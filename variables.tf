variable "s3_bucket_name" {
  type        = string
  description = "Name for S3 bucket."
}

variable "price_class" {
  type        = string
  default     = "PriceClass_All"
  description = "Cloudfront distribution price class. Either PriceClass_All, PriceClass_200 or PriceClass_100."
}

variable "domain_name" {
  type        = string
  default     = ""
  description = "Custom domain name. Will require acm_certificate_arn and route53_hosted_zone."
}

variable "route53_hosted_zone_name" {
  type        = string
  default     = null
  description = "Name of Route53 hosted zone."
}

variable "acm_certificate_domain_name" {
  type        = string
  default     = null
  description = "Domain name in ACM certificate."
}

variable "cloudfront_cache_policy_id" {
  type        = string
  default     = ""
  description = "Custom Cloudfront cache policy."
}

variable "http_version" {
  type        = string
  default     = "http3"
  description = "The maximum HTTP version to support on the distribution. Allowed values are http1.1, http2, http2and3 and http3"
}