provider "aws" {
  region = "us-east-1"
  alias  = "us-east-1"
}

resource "random_password" "referer" {
  length  = 13
  special = true
}

data "aws_acm_certificate" "acm_certificate" {
  count = var.domain_name == "" ? 0 : 1

  provider = aws.us-east-1

  domain = var.acm_certificate_domain_name

  statuses = [
    "ISSUED",
  ]
}

data "aws_route53_zone" "selected" {
  count = var.route53_hosted_zone_name == null ? 0 : 1

  name = var.route53_hosted_zone_name
}

###
#  S3 BUCKET
###
resource "aws_s3_bucket" "s3_bucket" {
  bucket = var.s3_bucket_name

  force_destroy = true
}

resource "aws_s3_bucket_website_configuration" "website_configuration" {
  bucket = aws_s3_bucket.s3_bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "index.html"
  }
}

resource "aws_s3_bucket_ownership_controls" "static_content" {
  bucket = aws_s3_bucket.s3_bucket.id

  rule {
    object_ownership = "BucketOwnerEnforced"
  }
}

###
#  S3 BUCKET POLICY
#  Restricting access to a specific HTTP referer
###
resource "aws_s3_bucket_policy" "s3_bucket_policy" {
  bucket = aws_s3_bucket.s3_bucket.id

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "Access restricted by a Referer header",
        "Effect" : "Allow",
        "Principal" : "*",
        "Action" : [
          "s3:GetObject"
        ],
        "Resource" : [
          "arn:aws:s3:::${var.s3_bucket_name}/*"
        ],
        "Condition" : {
          "StringLike" : { "aws:Referer" : [random_password.referer.result] }
        }
      }
    ]
  })
}

data "aws_cloudfront_cache_policy" "Managed-CachingOptimized" {
  name = "Managed-CachingOptimized"
}

#data "aws_cloudfront_origin_request_policy" "Managed-CachingOptimized" {
#  name = "Managed-AllViewer"
#}

resource "aws_cloudfront_distribution" "cloudfront_distribution" {
  origin {
    custom_origin_config {
      http_port              = "80"
      https_port             = "443"
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }

    domain_name = aws_s3_bucket_website_configuration.website_configuration.website_endpoint
    origin_id   = var.s3_bucket_name

    custom_header {
      name  = "Referer"
      value = random_password.referer.result
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Static website created by Terraform"
  default_root_object = "index.html"
  http_version        = var.http_version

  aliases = var.domain_name == "" ? null : [var.domain_name]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = var.s3_bucket_name

    cache_policy_id = var.cloudfront_cache_policy_id == "" ? data.aws_cloudfront_cache_policy.Managed-CachingOptimized.id : var.cloudfront_cache_policy_id

    compress = true

    viewer_protocol_policy = "redirect-to-https"
  }

  price_class = var.price_class

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn            = var.domain_name == "" ? null : data.aws_acm_certificate.acm_certificate[0].arn
    cloudfront_default_certificate = var.domain_name == "" ? true : null
    ssl_support_method             = var.domain_name == "" ? null : "sni-only"
    minimum_protocol_version       = var.domain_name == "" ? null : "TLSv1.2_2021"
  }
}

resource "aws_route53_record" "route53_record" {
  count = var.route53_hosted_zone_name == null ? 0 : 1

  zone_id = data.aws_route53_zone.selected[0].zone_id
  name    = var.domain_name
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.cloudfront_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.cloudfront_distribution.hosted_zone_id
    evaluate_target_health = true
  }
}
