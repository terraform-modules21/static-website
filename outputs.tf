output "s3_website_endpoint" {
  value = aws_s3_bucket_website_configuration.website_configuration.website_endpoint
}

output "cloudfront_dns" {
  value = aws_cloudfront_distribution.cloudfront_distribution.domain_name
}

output "cloudfront_id" {
  value = aws_cloudfront_distribution.cloudfront_distribution.id
}

output "s3_bucket_name" {
  value = aws_s3_bucket.s3_bucket.id
}
